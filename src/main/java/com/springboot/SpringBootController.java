package com.springboot;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpringBootController {
	@RequestMapping("/")
	public String home(Map<String,Object> model)
	{
		model.put("message","Welcome to index page222");
		System.out.println("hello master1");
		return "index1";
	}

	@RequestMapping("/next")
	public String index(Map<String,Object> model)
	{
		model.put("message","Welcome to next page");
		return "next";
	}
	
@Value("${welcome.message}")
private String messages = "";
	@RequestMapping("/welcome")
	public String welcome(Map<String,Object> model)
	{
		model.put("message",messages);
		return "welcome";
	}

}
